package ru.hnau.remote_teaching_common.utils

import ru.hnau.remote_teaching_common.data.ActionCodeType


object ActionCodeUtils {

    val SYMBOLS = "ABCDEFGHJKLMNPQRSTUVWXYZ23456789".toCharArray()

    fun generate(type: ActionCodeType) = String(
            (0 until type.codeLength).map { SYMBOLS[RANDOM.nextInt(SYMBOLS.size)] }.toCharArray()
    )

}