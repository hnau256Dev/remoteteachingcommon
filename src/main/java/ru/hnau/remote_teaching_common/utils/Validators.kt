package ru.hnau.remote_teaching_common.utils

import ru.hnau.remote_teaching_common.data.ActionCodeType
import ru.hnau.remote_teaching_common.exception.ApiException


object Validators {

    private val BASE_SUPPORTED_SYMBOLS = (('а'..'я') + ('А'..'Я') + ('a'..'z') + ('A'..'Z') + ('0'..'9') + "!@#$%^&*()_+=-;:,.<>{}[]/?№".toList()).toHashSet()
    private val BASE_SUPPORTED_SYMBOLS_WITH_SPACE = BASE_SUPPORTED_SYMBOLS + ' '

    const val MIN_LOGIN_LENGTH = 3
    const val MAX_LOGIN_LENGTH = 32
    val SUPPORTED_LOGIN_SYMBOLS = BASE_SUPPORTED_SYMBOLS

    fun validateUserLoginOrThrow(login: String) =
            validateString(login, "логина", MIN_LOGIN_LENGTH, MAX_LOGIN_LENGTH, SUPPORTED_LOGIN_SYMBOLS)


    const val MIN_PASSWORD_LENGTH = 3
    const val MAX_PASSWORD_LENGTH = 32
    val SUPPORTED_PASSWORD_SYMBOLS = BASE_SUPPORTED_SYMBOLS

    fun validateUserPasswordOrThrow(password: String) =
            validateString(password, "пароля", MIN_PASSWORD_LENGTH, MAX_PASSWORD_LENGTH, SUPPORTED_PASSWORD_SYMBOLS)


    const val MIN_NAME_LENGTH = 3
    const val MAX_NAME_LENGTH = 32
    val SUPPORTED_NAME_SYMBOLS = BASE_SUPPORTED_SYMBOLS_WITH_SPACE

    fun validateUserNameOrThrow(name: String) =
            validateString(name, "имени", MIN_NAME_LENGTH, MAX_NAME_LENGTH, SUPPORTED_NAME_SYMBOLS)


    const val MIN_SURNAME_LENGTH = 3
    const val MAX_SURNAME_LENGTH = 32
    val SUPPORTED_SURNAME_SYMBOLS = BASE_SUPPORTED_SYMBOLS_WITH_SPACE

    fun validateUserSurnameOrThrow(surname: String) =
            validateString(surname, "фамилии", MIN_SURNAME_LENGTH, MAX_SURNAME_LENGTH, SUPPORTED_SURNAME_SYMBOLS)


    const val MIN_PATRONYMIC_LENGTH = 3
    const val MAX_PATRONYMIC_LENGTH = 32
    val SUPPORTED_PATRONYMIC_SYMBOLS = BASE_SUPPORTED_SYMBOLS_WITH_SPACE

    fun validateUserPatronymicOrThrow(patronymic: String) =
            validateString(patronymic, "отчества", MIN_PATRONYMIC_LENGTH, MAX_PATRONYMIC_LENGTH, SUPPORTED_PATRONYMIC_SYMBOLS)


    const val MIN_STUDENTS_GROUP_NAME_LENGTH = 3
    const val MAX_STUDENTS_GROUP_NAME_LENGTH = 32
    val SUPPORTED_STUDENTS_GROUP_NAME_SYMBOLS = BASE_SUPPORTED_SYMBOLS_WITH_SPACE

    fun validateStudentsGroupNameOrThrow(studentsGroupName: String) =
            validateString(studentsGroupName, "названия группы", MIN_STUDENTS_GROUP_NAME_LENGTH, MAX_STUDENTS_GROUP_NAME_LENGTH, SUPPORTED_STUDENTS_GROUP_NAME_SYMBOLS)


    const val MIN_COURSE_NAME_LENGTH = 3
    const val MAX_COURSE_NAME_LENGTH = 32
    val SUPPORTED_COURSE_NAME_SYMBOLS = BASE_SUPPORTED_SYMBOLS_WITH_SPACE

    fun validateCourseNameOrThrow(courseName: String) =
            validateString(courseName, "названия курса", MIN_COURSE_NAME_LENGTH, MAX_COURSE_NAME_LENGTH, SUPPORTED_COURSE_NAME_SYMBOLS)

    fun validateActionCodeOrThrow(actionCode: String, actionCodeType: ActionCodeType) {
        if (actionCode.length != actionCodeType.codeLength) {
            throw ApiException.raw("Длина кода (${actionCode.length}) некорректна. Ожидаемая длина - ${actionCodeType.codeLength}")
        }
        actionCode.forEach {
            if (it !in ActionCodeUtils.SYMBOLS) {
                throw ApiException.raw("Недопустимый символ в коде - '$it'")
            }
        }
    }

    private fun validateString(
            string: String,
            name: String,
            minLength: Int,
            maxLength: Int,
            supportedSymbols: Set<Char>
    ) {
        if (string.length < minLength) {
            throw ApiException.raw("Минимальная длина $name - $minLength")
        }

        if (string.length > maxLength) {
            throw ApiException.raw("Максимальная длина $name - $maxLength")
        }

        string.forEach {
            if (it !in supportedSymbols) {
                throw ApiException.raw("Недопустимый символ в $name - '$it'")
            }
        }


    }

}