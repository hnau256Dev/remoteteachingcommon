package ru.hnau.remote_teaching_common.data.section


data class SectionSkeleton(
        val uuid: String = "",
        val parentUUID: String = "",
        val titleMD: String = ""
)