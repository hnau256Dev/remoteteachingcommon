package ru.hnau.remote_teaching_common.data.section


class OpenedSection(
        val studentsGroupName: String = "",
        val sectionUUID: String = "",
        val timestamp: Long = 0L
)