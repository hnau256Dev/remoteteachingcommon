package ru.hnau.remote_teaching_common.data


data class StudentsGroup(
        val name: String = "",
        val archived: Boolean = false
)