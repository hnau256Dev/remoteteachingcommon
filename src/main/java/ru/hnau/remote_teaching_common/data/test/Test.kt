package ru.hnau.remote_teaching_common.data.test

import ru.hnau.jutils.TimeValue


data class Test(
        val uuid: String = "",
        val sectionUUID: String = "",
        val maxAttemptsCount: Int = 3,
        val passRatingPercentage: Float = DEFAULT_PASS_RATING_PERCENTAGE,
        val summarizingExpectation: Long = DEFAULT_SUMMARIZING_EXPECTATION
) {

    companion object {

        const val DEFAULT_PASS_RATING_PERCENTAGE = 0.5f
        val DEFAULT_SUMMARIZING_EXPECTATION = TimeValue.DAY.milliseconds

    }
}