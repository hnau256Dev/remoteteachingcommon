package ru.hnau.remote_teaching_common.data.test


data class TestTask(
        val uuid: String = "",
        val testUUID: String = "",
        val maxScore: Float = 1f,
        val difficulty: Float = 0.5f,
        val type: TestTaskType = TestTaskType.SINGLE
)