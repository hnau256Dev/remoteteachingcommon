package ru.hnau.remote_teaching_common.data.test.additional_attempt


class AdditionalTestAttemptForStudentsGroup(
        val uuid: String = "",
        val testUUID: String = "",
        val studentsGroupName: String = "",
        val timestamp: Long = 0L
)