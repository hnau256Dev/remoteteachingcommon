package ru.hnau.remote_teaching_common.data.test


data class TestTaskVariant(
        val uuid: String = "",
        val taskUUID: String = "",
        val descriptionMD: String = "",
        val optionsMD: List<String> = emptyList(),
        val response: String = ""
)