package ru.hnau.remote_teaching_common.data.test.additional_attempt


class AdditionalTestAttemptForStudent(
        val uuid: String = "",
        val testUUID: String = "",
        val studentLogin: String = "",
        val timestamp: Long = 0L
)