package ru.hnau.remote_teaching_common.data.notification

import ru.hnau.remote_teaching_common.utils.GSON


sealed class RTNotification {

    data class Serialized(
            val className: String,
            val json: String
    )

    class Common(
            val message: String
    ) : RTNotification() {

        override fun generateDescription() =
                "message=$message"

    }

    companion object {

        fun deserialize(
                className: String,
                json: String
        ): RTNotification {
            val contentClassName = "${RTNotification::class.java.name}\$$className"
            val contentClass = Class.forName(contentClassName)
            return GSON.fromJson(json, contentClass) as RTNotification
        }

    }

    fun serialize() = Serialized(
            className = javaClass.simpleName,
            json = GSON.toJson(this)
    )

    protected open fun generateDescription(): String? = null

    override fun toString() =
            "${javaClass.simpleName}${generateDescription()?.let { "($it)" } ?: ""}"

}