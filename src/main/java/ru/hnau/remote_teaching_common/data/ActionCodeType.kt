package ru.hnau.remote_teaching_common.data

import ru.hnau.jutils.TimeValue


enum class ActionCodeType(
        val lifetime: TimeValue,
        val clearAfterCheck: Boolean,
        val codeLength: Int,
        val uniqueForAdditionalData: Boolean
) {

    CREATE_TEACHER(
            lifetime = TimeValue.DAY,
            clearAfterCheck = true,
            codeLength = 6,
            uniqueForAdditionalData = false
    ),

    CREATE_STUDENT_OF_GROUP(
            lifetime = TimeValue.WEEK,
            clearAfterCheck = false,
            codeLength = 5,
            uniqueForAdditionalData = false
    ),

    RESTORE_TEACHER_PASSWORD(
            lifetime = TimeValue.DAY,
            clearAfterCheck = true,
            codeLength = 6,
            uniqueForAdditionalData = true
    ),

    RESTORE_STUDENT_PASSWORD(
            lifetime = TimeValue.DAY,
            clearAfterCheck = true,
            codeLength = 6,
            uniqueForAdditionalData = true
    )


}