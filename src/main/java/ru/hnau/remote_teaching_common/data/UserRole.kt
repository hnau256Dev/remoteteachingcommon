package ru.hnau.remote_teaching_common.data


enum class UserRole(
        val permissions: Set<UserPermission> = emptySet()
) {

    ADMIN(
            permissions = UserPermission.values().toSet()
    ),

    TEACHER(
            permissions = hashSetOf(
                    UserPermission.MANAGE_STUDENTS,
                    UserPermission.MANAGE_SECTIONS,
                    UserPermission.MANAGE_GROUPS
            )
    ),

    STUDENT(
            permissions = emptySet()
    );

}