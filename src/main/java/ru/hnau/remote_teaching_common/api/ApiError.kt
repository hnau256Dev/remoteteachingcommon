package ru.hnau.remote_teaching_common.api


data class ApiError(
        val contentClass: String,
        val contentJson: String
)