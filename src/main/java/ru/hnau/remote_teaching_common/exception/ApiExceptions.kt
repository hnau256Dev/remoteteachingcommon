package ru.hnau.remote_teaching_common.exception

import ru.hnau.remote_teaching_common.api.ApiError
import ru.hnau.remote_teaching_common.utils.GSON
import java.lang.Exception


class ApiException(
        val content: ApiExceptionContent
) : Exception() {

    companion object {

        private fun create(content: ApiExceptionContent) =
                ApiException(content)

        val UNDEFINED = create(ApiExceptionContent.Undefined)

        val AUTHENTICATION = create(ApiExceptionContent.Authentication)

        val INCORRECT_ACTION_CODE = create(ApiExceptionContent.IncorrectActionCode)

        val ADMIN_PASSWORD_NOT_CONFIGURED = create(ApiExceptionContent.AdminPasswordNotConfigured)

        val SECTION_HAS_CHILDREN = create(ApiExceptionContent.SectionHasChildren)

        val NETWORK = create(ApiExceptionContent.Network)

        val USER_WITH_LOGIN_ALREADY_EXISTS = create(ApiExceptionContent.UserWithLoginAlreadyExists)

        fun raw(message: String) =
                create(ApiExceptionContent.Common(message))

        fun ddosBlockedIp(secondsToUnlock: Long?) =
                create(ApiExceptionContent.DdosBlockedIp(secondsToUnlock))

        fun ddosBlockedUser(secondsToUnlock: Long?) =
                create(ApiExceptionContent.DdosBlockedUser(secondsToUnlock))

        fun unsupportedVersion(oldVersion: Int, newVersion: Int, comment: String, updateUrl: String? = null) =
                create(ApiExceptionContent.UnsupportedVersion(oldVersion, newVersion, comment, updateUrl))

        fun deserialize(
                apiError: ApiError
        ) = ApiException(
                content = run {
                    val contentClassName = "${ApiExceptionContent::class.java.name}\$${apiError.contentClass}"
                    val clazz = Class.forName(contentClassName)
                    GSON.fromJson(apiError.contentJson, clazz) as ApiExceptionContent
                }
        )

    }

    fun serialize() = ApiError(
            contentClass = content.javaClass.simpleName,
            contentJson = GSON.toJson(content)
    )

    override fun toString() =
            "ApiException(content=$content})"

}